# -*- coding: utf-8 -*-
import numpy as np
from math import radians,pi


def _shadowing(a,azimuth,altitude,scale,vegdem=None,vegdem2=None,bush=None,dlg=None,forsvf=1):    
    if vegdem is None:
        use_veg=False
    else: 
        use_veg=True

    if azimuth == 0.0:
        azimuth = 0.000000000001
    azimuth = np.deg2rad(azimuth)
    altitude = np.deg2rad(altitude)

    #% measure the size of the image
    sizex = a.shape[0]
    sizey = a.shape[1]
    if forsvf == 0:
        barstep = np.max([sizex, sizey])
        dlg.progressBar.setRange(0, barstep)
        dlg.progressBar.setValue(0)
    #% initialise parameters
    
    dx = 0.
    dy = 0.
    dz = 0.
    
    temp = np.zeros_like(a)
    sh = np.zeros((sizex, sizey))
    f = a
    
    if use_veg:
        if bush is None:
            bush=np.zeros_like(vegdem)
        tempvegdem = np.zeros_like(vegdem)
        tempvegdem2 = np.zeros_like(vegdem)
        shvoveg = np.copy(vegdem) 
        vbshvegsh = np.zeros_like(vegdem)
        vegsh = np.zeros_like(vegdem)
        tempbush =np.zeros_like(vegdem)
        g = np.zeros_like(a)
        bushplant = bush > 1.
    
    #% other loop parameters
    amaxvalue = a.max()
    pibyfour = pi/4.
    threetimespibyfour = 3*pibyfour
    fivetimespibyfour = 5*pibyfour
    seventimespibyfour = 7*pibyfour
    sinazimuth = np.sin(azimuth)
    cosazimuth = np.cos(azimuth)
    tanazimuth = np.tan(azimuth)
    signsinazimuth = np.sign(sinazimuth)
    signcosazimuth = np.sign(cosazimuth)
    dssin = np.abs((1./sinazimuth))
    dscos = np.abs((1./cosazimuth))
    tanaltitudebyscale = np.tan(altitude) / scale
    
    #% main loop
    index = 1
    while (amaxvalue >= dz and np.abs(dx) < sizex and np.abs(dy) < sizey):
        if forsvf == 0:
            dlg.progressBar.setValue(index)
    #while np.logical_and(np.logical_and(amaxvalue >= dz, np.abs(dx) <= sizex), np.abs(dy) <= sizey):(np.logical_and(amaxvalue >= dz, np.abs(dx) <= sizex), np.abs(dy) <= sizey):
        #if np.logical_or(np.logical_and(pibyfour <= azimuth, azimuth < threetimespibyfour), np.logical_and(fivetimespibyfour <= azimuth, azimuth < seventimespibyfour)):
        if (pibyfour <= azimuth and azimuth < threetimespibyfour or fivetimespibyfour <= azimuth and azimuth < seventimespibyfour):
            dy = signsinazimuth * index
            dx = -1. * signcosazimuth * np.abs(np.round(index / tanazimuth))
            ds = dssin
        else:
            dy = signsinazimuth * np.abs(np.round(index * tanazimuth))
            dx = -1. * signcosazimuth * index
            ds = dscos

        #% note: dx and dy represent absolute values while ds is an incremental value
        dz = ds *index * tanaltitudebyscale
        temp[0:sizex, 0:sizey] = 0.
        if use_veg:
            tempvegdem[0:sizex, 0:sizey] = 0.
            tempvegdem2[0:sizex, 0:sizey] = 0.

        absdx = np.abs(dx)
        absdy = np.abs(dy)
        xc1 = int((dx+absdx)/2)
        xc2 = int(sizex+(dx-absdx)/2)
        yc1 = int((dy+absdy)/2)
        yc2 = int(sizey+(dy-absdy)/2)

        xp1 = -int((dx-absdx)/2)
        xp2 = int(sizex-(dx+absdx)/2)
        yp1 = -int((dy-absdy)/2)
        yp2 = int(sizey-(dy+absdy)/2)
        temp[int(xp1):int(xp2), int(yp1):int(yp2)] = a[int(xc1):int(xc2), int(yc1):int(yc2)] - dz
        f = np.maximum(f, temp)

        if use_veg:
            tempvegdem[int(xp1):int(xp2), int(yp1):int(yp2)] = vegdem[int(xc1):int(xc2), int(yc1):int(yc2)] - dz
            tempvegdem2[int(xp1):int(xp2), int(yp1):int(yp2)] = vegdem2[int(xc1):int(xc2), int(yc1):int(yc2)] - dz
            shvoveg = np.max([shvoveg, tempvegdem], axis=0)
            sh[(f > a)] = 1.
            sh[(f <= a)] = 0.
            #%Moving building shadow
            fabovea = (tempvegdem > a).astype(int)
            #%vegdem above DEM
            gabovea = (tempvegdem2 > a).astype(int)
            #%vegdem2 above DEM
            # vegsh2 = np.float(fabovea)-np.float(gabovea)
            vegsh2 = np.subtract(fabovea, gabovea, dtype=np.float)
            vegsh = np.maximum(vegsh, vegsh2)
            vegsh[(vegsh*sh > 0.)] = 0.
            #% removing shadows 'behind' buildings
            vbshvegsh = vegsh+vbshvegsh
            #% vegsh at high sun altitudes
            if index == 1.:
                firstvegdem = tempvegdem-temp
                firstvegdem[(firstvegdem <= 0.)] = 1000.
                vegsh[(firstvegdem < dz)] = 1.
                vegsh = vegsh*(vegdem2 > a)
                vbshvegsh = np.zeros((sizex, sizey))

            #% Bush shadow on bush plant
            if np.max(bush) > 0 and np.max(fabovea*bush) > 0:
                tempbush[0:sizex, 0:sizey] = 0.
                tempbush[int(xp1):int(xp2), int(yp1):int(yp2)] = bush[int(xc1)-1:int(xc2),int(yc1)-1:int(yc2)]-dz
                g = np.maximum(g, tempbush)
                g *= bushplant
        
        index += 1.
    if use_veg:
        vbshvegsh[(vbshvegsh > 0.)] = 1.
        vbshvegsh = vbshvegsh-vegsh

        if bush.max() > 0.:
            g = g-bush
            g[(g > 0.)] = 1.
            g[(g < 0.)] = 0.
            vegsh = vegsh-bushplant+g
            vegsh[(vegsh<0.)] = 0.

        vegsh[(vegsh > 0)] = 1
        shvoveg = (shvoveg-a) * vegsh
        vegsh = 1-vegsh
        vbshvegsh = 1-vbshvegsh
    else:
        vegsh=None
        vbshvegsh =None
        shvoveg = None

    f = f-a

    if not use_veg:
        return f
    else:
        return {'sh': f, 'vegsh': vegsh, 'vbshvegsh': vbshvegsh,'shvoveg':shvoveg}