# -*- coding: utf-8 -*-
# Ready for python action!
import numpy as np
from math import radians,pi
# import matplotlib.pylab as plt

from .shadowing import _shadowing


def build_shadowmap(shadowvol,a):
    shadowvol = np.logical_not(shadowvol)
    sh = np.double(shadowvol)
    return sh

def build_wall_shadows(shadowvol, a, azimuth, altitude,  walls, aspect):
    wallbol = (walls > 0).astype(float)
    azimuth = np.deg2rad(azimuth)
    altitude = np.deg2rad(altitude)

    # Removing walls in shadow due to selfshadowing
    azilow = azimuth-pi/2
    azihigh = azimuth+pi/2
    
    if azilow >= 0 and azihigh < 2*np.pi:    # 90 to 270  (SHADOW)
        facesh = (np.logical_or(aspect < azilow, aspect >= azihigh).astype(float)-wallbol+1)
    elif azilow < 0 and azihigh <= 2*np.pi:    # 0 to 90
        azilow = azilow + 2*np.pi
        facesh = np.logical_or(aspect > azilow, aspect <= azihigh) * -1 + 1    # (SHADOW)    # check for the -1
    elif azilow > 0 and azihigh >= 2*np.pi:    # 270 to 360
        azihigh = azihigh-2*np.pi
        facesh = np.logical_or(aspect > azilow, aspect <= azihigh)*-1 + 1    # (SHADOW)

    facesun = np.logical_and(facesh + (walls > 0).astype(float) == 1, walls > 0).astype(float)
    wallsun = np.copy(walls-shadowvol)
    wallsun[wallsun < 0] = 0
    wallsun[facesh == 1] = 0    # Removing walls in "self"-shadow
    wallsh = np.copy(walls-wallsun)
    
    return wallsh, wallsun, facesh, facesun

def shadowing(a, azimuth, altitude, scale, dlg, forsvf):
    shadowvol = _shadowing(a,azimuth,altitude,scale,dlg=dlg,forsvf=forsvf)

    return build_shadowmap(shadowvol,a)

def shadowing_veg(a, vegdem, vegdem2, azimuth, altitude, scale, amaxvalue, bush, dlg, forsvf,return_shadowmap=True):
    shadow_data = _shadowing(a,azimuth,altitude,scale,vegdem,vegdem2,bush,dlg,forsvf=1)
    if return_shadowmap:
        shadow_data['sh']=build_shadowmap(shadow_data['sh'],a)
    return shadow_data

def shadowing_walls(a, azimuth, altitude, scale, walls, aspect):
    
    shadowvol=_shadowing(a, azimuth, altitude, scale)
    wallsh, wallsun, facesh, facesun = build_wall_shadows(shadowvol,a,azimuth,altitude,walls,aspect)
    sh = build_shadowmap (shadowvol,a)
    return sh, wallsh, wallsun, facesh, facesun
    


def shadowing_walls_veg(a, vegdem, vegdem2, azimuth, altitude, scale, amaxvalue, bush, walls, aspect):
    wallbol = (walls > 0).astype(float)
    shadow_data = shadowing_veg(a, vegdem, vegdem2, azimuth, altitude, scale, amaxvalue, bush, dlg=None, forsvf=1,return_shadowmap=False)

    shadow_vol = shadow_data['sh']
    vegsh = shadow_data['vegsh']
    vbshvegsh = shadow_data['vbshvegsh']
    shvoveg = shadow_data['shvoveg']
 
    wallsh, wallsun, facesh, facesun = build_wall_shadows(shadow_vol,a,azimuth,altitude,walls,aspect)
    sh = build_shadowmap (shadow_vol,a)
    wallshve = shvoveg * wallbol
    wallshve = wallshve - wallsh
    wallshve[wallshve < 0] = 0
    idx = np.where(wallshve > walls)
    wallshve[idx] = walls[idx]
    wallsun = wallsun-wallshve    # problem with wallshve only
    idx = np.where(wallsun < 0)
    wallshve[idx] = 0
    wallsun[idx] = 0
    

    return vegsh, sh, vbshvegsh, wallsh, wallsun, wallshve, facesh, facesun

shadowingfunctionglobalradiation=shadowing
shadowingfunction_20=shadowing_veg