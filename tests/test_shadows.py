import unittest
import numpy as np
from Utilities import shadowingfunctions

TESTDATA_FILE = "tests/data/shadowtest_dem.npz"
SHADOW_RESULTS_FILE = "tests/data/shadowtest_shadow_results.npz"
VEG_SHADOW_RESULTS_FILE = "tests/data/shadowtest_shadowveg_results.npz"


class TestShadow(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        input_data=np.load(TESTDATA_FILE)
        self.dsm = input_data['dsm']
        self.veg = input_data['veg']
        self.dsm_max_value=self.dsm.max()

        self.test_directions=["30_06","180_60","307_30"]

        tree_heights=self.veg-self.dsm
        tree_heights[self.veg==0]=0
        self.vegdem2=tree_heights*0.25
        self.vegdem2+=self.dsm
        self.vegdem2[self.veg==0]=0
        self.bush=np.zeros_like(self.veg)
        self.bush[50:55,70:75]=self.dsm[50:55,70:75]+3
        
    def test_shadow(self):
        shadow_results=np.load(SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            sh = shadowingfunctions.shadowing(self.dsm,azimuth,altitude,0.5,None,1 )   
            expected_result=shadow_results['sh_'+dirs]
            self.assertTrue(np.allclose(sh,expected_result))
    def test_vegshadow_noveg(self):
        shadow_results=np.load(SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            sh = shadowingfunctions.shadowing_veg(self.dsm,self.veg,self.vegdem2,azimuth,altitude,0.5,self.dsm_max_value,self.bush,None,1 )   
            expected_result=shadow_results['sh_'+dirs]
            self.assertTrue(np.allclose(sh['sh'],expected_result))
    def test_vegshadow(self):
        shadow_results=np.load(VEG_SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            sh = shadowingfunctions.shadowing_veg(self.dsm,self.veg,self.vegdem2,azimuth,altitude,0.5,self.dsm_max_value,self.bush,None,1 )   
            expected_result=shadow_results['vegsh_'+dirs]
            self.assertTrue(np.allclose(sh['vegsh'],expected_result))

    def test_vbvegshadow(self):
        shadow_results=np.load(VEG_SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            sh = shadowingfunctions.shadowing_veg(self.dsm,self.veg,self.vegdem2,azimuth,altitude,0.5,self.dsm_max_value,self.bush,None,1 )   
            expected_result=shadow_results['vbshvegsh_'+dirs]
            self.assertTrue(np.allclose(sh['vbshvegsh'],expected_result))
