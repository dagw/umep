import unittest
import numpy as np
from Utilities import shadowingfunctions

TESTDATA_FILE = "tests/data/shadowtest_dem.npz"
TEST_WALLDATA_FILE = "tests/data/wall_data.npz"
SHADOW_RESULTS_FILE = "tests/data/shadowtest_shadow_results.npz"
VEG_SHADOW_RESULTS_FILE = "tests/data/shadowtest_shadowveg_results.npz"


WALLSHADOWS_RESULTS = "tests/data/wall_shadows.npz"
VEG_WALLSHADOWS_RESULTS = "tests/data/wall_shadows_veg.npz"



class TestWallShadow(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        input_data=np.load(TESTDATA_FILE)
        input_wall_data=np.load(TEST_WALLDATA_FILE)
        self.dsm = input_data['dsm']
        self.veg = input_data['veg']
        self.wall_height= input_wall_data['wall_height']
        self.wall_angle= input_wall_data['wall_angle']
        self.dsm_max_value=self.dsm.max()

        self.test_directions=["30_06","180_60","307_30"]

        tree_heights=self.veg-self.dsm
        tree_heights[self.veg==0]=0
        self.vegdem2=tree_heights*0.25
        self.vegdem2+=self.dsm
        self.vegdem2[self.veg==0]=0
        self.bush=np.zeros_like(self.veg)
        self.bush[50:55,70:75]=self.dsm[50:55,70:75]+3

        self.wall_shadowing_values={}
        self.wall_veg_shadowing_values={}
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            self.wall_shadowing_values[dirs]=shadowingfunctions.shadowing_walls(self.dsm,azimuth,altitude,0.5,self.wall_height,self.wall_angle)
            self.wall_veg_shadowing_values[dirs]=shadowingfunctions.shadowing_walls_veg \
                (self.dsm, self.veg, self.vegdem2, azimuth, altitude, 0.5, self.dsm_max_value, 
                self.bush, self.wall_height, self.wall_angle)

    def test_wallsh(self):
        wall_shadow_results=np.load(WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _,wallsh,_,_,_=self.wall_shadowing_values[dirs]
            expected_result=wall_shadow_results["wallsh_"+dirs]
            self.assertTrue(np.allclose(wallsh,expected_result))
    def test_wallsun(self):
        wall_shadow_results=np.load(WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _,_,wallsun,_,_=self.wall_shadowing_values[dirs]
            expected_result=wall_shadow_results["wallsun_"+dirs]
            self.assertTrue(np.allclose(wallsun,expected_result))

    def test_facesh(self):
        wall_shadow_results=np.load(WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _,_,_,facesh,_=self.wall_shadowing_values[dirs]
            expected_result=wall_shadow_results["facesh_"+dirs]
            self.assertTrue(np.allclose(facesh,expected_result))
    
    def test_facesun(self):
        wall_shadow_results=np.load(WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _,_,_,_,facesun=self.wall_shadowing_values[dirs]
            expected_result=wall_shadow_results["facesun_"+dirs]
            self.assertTrue(np.allclose(facesun,expected_result))

    def test_vegsh(self):
        wall_shadow_results=np.load(VEG_SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            vegsh, _, _, _, _, _, _, _=self.wall_veg_shadowing_values[dirs]
            expected_result=wall_shadow_results["vegsh_"+dirs]
            self.assertTrue(np.allclose(vegsh,expected_result))

    def test_sh_veg(self):
        wall_shadow_results=np.load(SHADOW_RESULTS_FILE)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, sh, _, _, _, _, _, _=self.wall_veg_shadowing_values[dirs]
            expected_result=wall_shadow_results["sh_"+dirs]
            self.assertTrue(np.allclose(sh,expected_result))

    
    
    def test_vbshvegsh(self):
        veg_wall_shadow_results=np.load(VEG_WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, _, vbshvegsh, _, _, _, _, _  =self.wall_veg_shadowing_values[dirs]
            expected_result=veg_wall_shadow_results["vbshvegsh_"+dirs]
            self.assertTrue(np.allclose(vbshvegsh,expected_result))
    
    def test_wallsh_veg(self):
        wall_shadow_results=np.load(VEG_WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, _, _, wallsh, _, _, _, _  =self.wall_veg_shadowing_values[dirs]
            expected_result=wall_shadow_results["wallsh_"+dirs]
            self.assertTrue(np.allclose(wallsh,expected_result))

    def test_wallsun_veg(self):
        wall_shadow_results=np.load(VEG_WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, _, _, _, wallsun, _, _, _  =self.wall_veg_shadowing_values[dirs]
            expected_result=wall_shadow_results["wallsun_"+dirs]
            self.assertTrue(np.allclose(wallsun,expected_result))
            

    def test_wallshve(self):
        veg_wall_shadow_results=np.load(VEG_WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, _, _, _, _, wallshve, _, _  =self.wall_veg_shadowing_values[dirs]
            expected_result=veg_wall_shadow_results["wallshve_"+dirs]
            self.assertTrue(np.allclose(wallshve,expected_result))
    
    
    
    def test_facesun_veg(self):
        wall_shadow_results=np.load(WALLSHADOWS_RESULTS)
        for dirs in self.test_directions:
            azimuth, altitude = map(int,dirs.split('_'))
            _, _, _, _, _, _, _, facesun  =self.wall_veg_shadowing_values[dirs]
            expected_result=wall_shadow_results["facesun_"+dirs]
            self.assertTrue(np.allclose(facesun,expected_result))
    
